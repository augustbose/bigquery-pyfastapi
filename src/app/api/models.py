from pydantic import BaseModel


class Action(BaseModel):
    type: str
    sub_type: str = None


class Log(BaseModel):
    app_name: str
    userid: str
    more_info: str
    timestamp: str
    action: Action
